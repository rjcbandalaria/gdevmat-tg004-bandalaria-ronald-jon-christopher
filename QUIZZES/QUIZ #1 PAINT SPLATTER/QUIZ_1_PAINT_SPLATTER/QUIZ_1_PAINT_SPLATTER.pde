void setup(){
  size(1920,1080,P3D);
  camera(0,0, -(height / 2.0) / tan(PI * 30 / 180), 0, 0, 0, 0 ,-1 ,0);
  background(0);
  frameCount = 0; 
}

void draw(){
  
  float gauss = randomGaussian();
  float standardDeviation = 250;
  float mean = 0; 
  float randomCircleSize = random(1,100);
  float xPosition = (standardDeviation * gauss)+ mean;
  float yPosition = random(-1080,1080);
  float circleSize = gauss + randomCircleSize;
  float redColor = random(0,256);
  float greenColor = random(0, 256);
  float blueColor = random (0, 256);
  float alphaValue = random(0,256);
  
  
  noStroke();
  fill(redColor,greenColor,blueColor,alphaValue);
  circle(xPosition,yPosition,circleSize);
  frameCount++;
  
  if(frameCount ==1000){
    setup();  
  }
  
  
}
