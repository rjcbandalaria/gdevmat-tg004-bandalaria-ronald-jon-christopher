class Mover{
  
  public PVector position = new PVector();
  public PVector velocity = new PVector();
  public PVector acceleration = new PVector();
  public float scale = 50; 
  public float mass = 1; 
  public float red;
  public float green; 
  public float blue; 
  public float alpha;
  
    Mover()
   {

   }
   
   Mover(float x, float y)
   {
      position = new PVector(x, y);
   }
   
   
   Mover(float x, float y, float scale)
   {
      position = new PVector(x, y);
   }
   
   Mover(PVector position)
   {
      this.position = position;
   }
   
  public void render(){
    
    noStroke();
    println(position.x);
    fill(red, green, blue, alpha);
    circle(position.x, position.y, this.mass * 10);
    
  }
    public void setColor(float r, float g, float b, float a)
   {
      this.red = r;
      this.green = g;
      this.blue = b;
      this.alpha = a;
   }
  
  private void update(){
    
    this.velocity.add(this.acceleration);
    this.velocity.limit(30);
    this.position.add(this.velocity);
    this.acceleration.mult(0);
  
  }
    
  public void applyForce(PVector force){
   
    //A = F/M
    PVector f = PVector.div(force, this.mass);
    this.acceleration.add(f);
    
  }
  
  
  
}
