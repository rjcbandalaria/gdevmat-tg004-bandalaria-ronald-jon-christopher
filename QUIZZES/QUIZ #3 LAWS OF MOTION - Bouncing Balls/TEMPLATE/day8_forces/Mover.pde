public class Mover
{
   public PVector position = new PVector();
   public PVector velocity = new PVector();
   public PVector acceleration = new PVector();
   
   public float mass = 1;
   public float r = 255, g = 255, b = 255, a = 255;
   
   Mover()
   {

   }
   
   Mover(float x, float y)
   {
      position = new PVector(x, y);
   }
   
   
   Mover(float x, float y, float scale)
   {
      position = new PVector(x, y);
   }
   
   Mover(PVector position)
   {
      this.position = position;
   }
   
   public void render()
   {
      fill(r,g,b,a);
      circle(position.x, position.y, this.mass * 10); 
   }
   
   public void setColor(float r, float g, float b, float a)
   {
      this.r = r;
      this.g = g;
      this.b = b;
      this.a = a;
   }
   
   public void update()
   {
      // we add the acceleration to the velocity every frame
      this.velocity.add(this.acceleration);
      
      this.velocity.limit(30);
     
      //we add the velocity to the position every frame
      this.position.add(this.velocity);
      
      this.acceleration.mult(0);
   }
   
   
   public void applyForce(PVector force)
   {
     PVector f = PVector.div(force, this.mass); // F = M / A
     this.acceleration.add(f); // accumulate force every frame
   }
}
