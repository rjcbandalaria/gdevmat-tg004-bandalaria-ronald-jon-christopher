class Walker {

  float xPosition;
  float yPosition;
  float diameter;
  float red = 0;
  float green = 0; 
  float blue = 0; 
 
  
  void render(){

    noStroke(); 
    circle(xPosition, yPosition, diameter);

  }
  
  float xDt = 0;
  float yDt = 50;    
  float diameterDt = 0;
  float redDt = 100;
  float greenDt = 200;
  float blueDt = 150;
  
  void randomWalk(){

    xPosition = map(noise(xDt), 0, 1, Window.left, Window.right);
    yPosition = map(noise(yDt), 0, 1, Window.bottom, Window.top);
    diameter = map(noise(diameterDt), 0 ,1, 10,100); 
    
    red = map(noise(redDt), 0,1, 0, 255);
    green = map(noise(greenDt), 0, 1, 0, 255);
    blue = map(noise(blueDt), 0 , 1, 0 ,255);
    
    xDt += 0.01f;
    yDt += 0.01f;
    diameterDt += 0.01;
    redDt +=0.1;
    greenDt += 0.1;
    blueDt += 0.1;
   
    fill (red, green, blue, 255);
    
  }
    
}
