public class SpaceMatter{
  
  public PVector position;
  public float scale; 
  public float red;
  public float blue;
  public float green; 
  
  SpaceMatter(){
   
    position = new PVector();
    
  }
  
  SpaceMatter (float x, float y){
   
   position = new PVector(x,y);
    
  }
  
  SpaceMatter(float x, float y, float matterSize){
   
    position = new PVector(x,y);
    scale = matterSize;
    
  }
  
  public void setSpaceMatterColor(){
    
   red = random(256);
   green = random(256);
   blue = random(256);
    
    
  }
  
  SpaceMatter(PVector position){
  
    this.position= position; 
    
  }
  
  SpaceMatter(PVector position, float matterSize){
   
    this.position = position;
    this.scale = matterSize;
    
  }
  
  
  public void setSpaceMatterSize(){

   scale = random(5,27);
    
  }
  
  public void spawnSpaceMatter(){
   
    noStroke();
    fill(red,green, blue);
    circle(position.x, position.y, scale);
    
  }
  
  
}
