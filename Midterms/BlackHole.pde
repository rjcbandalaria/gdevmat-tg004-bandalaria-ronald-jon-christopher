public class BlackHole{
  
  public PVector position;
  public float scale = 50; 
  public float red;
  public float blue;
  public float green; 
  
  BlackHole(){
   
    position = new PVector();
    
  }
  
  BlackHole (float x, float y){
   
   position = new PVector(x,y);
    
  }
  
  public void setBlackHoleColor(){
    
   red = 255;
   green = 255;
   blue = 255; 
    
  }
 
  public void spawnBlackHole(){
   
    fill(red,green,blue);
    circle(position.x, position.y, scale);
    
  }
  
  
}
