void setup()
{
  size(1920, 1080, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  background(0);
  frameCount = 0;
  createSpaceMatter();
  
}
Window window = new Window();
BlackHole blackHole = new BlackHole(random(window.left, window.right), random(window.bottom, window.top));
int numOfSpaceMatter = 100;
SpaceMatter spaceMatter[] = new SpaceMatter[numOfSpaceMatter];

void createSpaceMatter(){
  
  for(int i = 0; i < numOfSpaceMatter; i++){
    
    float gauss = randomGaussian();
    float standardDev = 200; 
    float meanXPosition = random(window.left, window.right);//window.right / window.left;
    float meanYPosition = random(window.bottom, window.top);
    float xPosition = (standardDev * gauss) + meanXPosition; 
    float yPosition = (standardDev * gauss) + meanYPosition;
    
     spaceMatter[i] = new SpaceMatter(xPosition, yPosition);
     spaceMatter[i].setSpaceMatterColor();
     spaceMatter[i].setSpaceMatterSize();
     spaceMatter[i].spawnSpaceMatter();
     
   }
  
}

void createBlackHole(){
 
  blackHole = new BlackHole(random(window.left, window.right), random(window.bottom, window.top));
  blackHole.setBlackHoleColor();
  blackHole.spawnBlackHole();
  
}

void pullSpaceMatter(){
  
   for(int i = 0; i < numOfSpaceMatter; i++){
   
    spaceMatter[i].spawnSpaceMatter();
    PVector direction = PVector.sub(blackHole.position, spaceMatter[i].position);
    direction.normalize();
    direction.mult(20);
    spaceMatter[i].position.add(direction);
  
  }
  
}

void draw (){
  background(0);
  frameCount++;
  
  pullSpaceMatter();
  blackHole.setBlackHoleColor();
  blackHole.spawnBlackHole();
  
  if(frameCount >= 200){
   background(0);
   setup();
   createBlackHole();

 }
  
}
