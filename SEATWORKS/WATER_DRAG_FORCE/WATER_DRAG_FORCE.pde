int numOfBalls = 10;
Mover balls[] = new Mover[numOfBalls];
Window window;
Liquid ocean = new Liquid(0, -100, window.right, window.bottom, 0.1);

void setup(){
  size(1920,1080,P3D);
  camera(0,0, -(height / 2.0) / tan(PI * 30 / 180), 0, 0, 0, 0 ,-1 ,0);
  
  createBalls();
 
}

void createBalls(){
  
   for(int i = 0; i < numOfBalls; i++){
   
   float colorRed = random(1,255);
   float colorGreen = random(1,255);
   float colorBlue = random(1,255);
   float alpha = random(150,255);
   
   balls[i] = new Mover(Window.right - (180* (i+1)), window.top);
   balls[i].setColor(colorRed, colorGreen, colorBlue, alpha);
   balls[i].mass = i +2;
  
  }
}

PVector wind = new PVector(0.1f, 0);


void draw(){
  
 background(255);

 ocean.render();
 noStroke();
 
 for(Mover ball : balls){
  
   ball.applyGravity();
  
  if(!ocean.isCollidingWith(ball)){
    
     ball.applyForce(wind);

   }
  
  ball.applyFriction();
  
  if(ball.position.x > window.right){
   
    ball.velocity.x *= -1;
    ball.position.x = window.right;
    
  }
  
  if(ball.position.y < window.bottom){
   
    ball.velocity.y *= -1;
    ball.position.y = window.bottom;
    
  }
  
  if(ocean.isCollidingWith(ball)){
   
    ball.applyForce(ocean.calculateDragForce(ball));
    
  }
  
  ball.update();
  ball.render();

 }
}
