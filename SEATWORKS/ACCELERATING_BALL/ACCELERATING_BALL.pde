Mover ball;
Window window;

void setup(){
  size(1920,1080,P3D);
  camera(0,0, -(height / 2.0) / tan(PI * 30 / 180), 0, 0, 0, 0 ,-1 ,0);
  
  ball = new Mover();
  //ball.velocity = new PVector(10,25);
  ball.position.x = window.left+50;
  ball.acceleration = new PVector (0.1,0);


  
}

void draw(){
  
 background(255);
 
 fill(200,0,0);

// println(ball.velocity);


 println(ball.velocity.mag());
 
 if (ball.position.x >= 0){
  
  ball.velocity.add(-.2,0);
   
  if (ball.velocity.x <= 0){
    
    ball.acceleration.x = 0;
    ball.velocity.x = 0; 
    
  }
   
 }

  ball.update();
   ball.render();
}
