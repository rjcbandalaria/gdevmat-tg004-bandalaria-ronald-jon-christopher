class Mover{
  
  public PVector position = new PVector();
  public PVector velocity = new PVector();
  public PVector acceleration = new PVector();
  public float scale = 50; 
  
  public void render(){
    
 
    println(position.x);
    circle(position.x, position.y, scale);
    
  }
  
  private void update(){
    
    this.velocity.add(this.acceleration);
    this.velocity.limit(10);
    this.position.add(this.velocity);
  
  }
    
  
}
