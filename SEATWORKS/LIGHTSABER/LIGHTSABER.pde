void setup(){
  size(1920,1080,P3D);
  camera(0,0, -(height / 2.0) / tan(PI * 30 / 180), 0, 0, 0, 0 ,-1 ,0);

}

Vector2 mousePos(){
 
  float x = mouseX - Window.windowWidth;
  float y = -(mouseY - Window.windowHeight);
  
  return new Vector2(x,y);
}

float redDt = 1;
float greenDt = 2;
float blueDt = 15;

float lightsaberAngleDt = 0;


void draw(){
  background(0);
  
  float red = map(noise(redDt), 0,1, 0,255);
  float green = map(noise(greenDt), 0 ,1, 0, 255);
  float blue = map(noise(blueDt), 0, 1 ,0, 255);
  float lightsaberMovement = map(noise(lightsaberAngleDt), 0, 1, 0, 2*PI);

  Vector2 mouse = mousePos();
  
  // First blade glow 
  mouse.normalized();
  mouse.multiply(600);
  
  strokeWeight(15);
  stroke(red,green,blue);
  rotate(lightsaberMovement);  
  line(0,0,mouse.x,mouse.y);
  
  //Second Blade glow 
  mouse.normalized();
  mouse.multiply(-600);
  
  strokeWeight(15);
  stroke(red,green,blue);
  line(0,0,mouse.x,mouse.y);
  
  // First blade 
  mouse.normalized();
  mouse.multiply(596);
  
  strokeWeight(8);
  stroke(255,255,255,140);
  line(0,0,mouse.x,mouse.y);
  
  // Second blade 
  mouse.normalized();
  mouse.multiply(-596);
  
  strokeWeight(8);
  stroke(255,255,255,140);
  line(0,0,mouse.x,mouse.y);
  
  // Hilt 1st part 
  mouse.normalized();
  mouse.multiply(100);
  
  strokeWeight(20);
  stroke(192,192,192, 255);
  line(0,0,mouse.x,mouse.y);
  
  // Hilt 2nd part 
  mouse.normalized();
  mouse.multiply(-100);
  
  strokeWeight(20);
  stroke(192,192,192, 255);
  line(0,0,mouse.x,mouse.y);
  
  //Saber Guard 
  mouse.normalized();
  stroke(16,16,16, 255);
  circle(0,0,190);
  
  //Switch 1 
  mouse.normalized();
  mouse.multiply(5);
  
  strokeWeight(5);
  stroke(255,0,0, 255);
  line(0,0,mouse.x,mouse.y);
 
  noFill();
  
  redDt+=.1;
  greenDt+=.2;
  blueDt+=.4;
  lightsaberAngleDt += .02;
  
  println(mouse.mag());
}
