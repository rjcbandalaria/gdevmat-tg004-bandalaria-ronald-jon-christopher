public class Vector2{
  
 public float x;
 public float y;
 
 Vector2(){
   
 this.x=0;
 this.y=0;
   
 }
 
 Vector2(float x, float y){
   
  this.x = x;
  this.y = y;
   
 }
 
 public void add(Vector2 u){
  
   this.x += u.x;
   this.y += u.y;
   
   
 }
 
 public float mag(){
  
   return sqrt((x*x) + (y*y));
   
 }
 
 public Vector2 multiply(float scalar){
   
   this.x *= scalar;
   this.y *= scalar;
   
   return this;
   
   
 }
 
 public Vector2 divide(float scalar){
   
  this.x /= scalar; 
  this.y /= scalar;
  
  return this; 
   
 }
 
 
 public Vector2 normalized(){
   
   float length = this.mag();
   
   if(length > 0){
    
     this.divide(length);
     
   }
   return this;
   
 }
  
}
