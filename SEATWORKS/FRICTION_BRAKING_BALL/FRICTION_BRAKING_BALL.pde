int numOfBalls = 5;
Mover balls[] = new Mover[numOfBalls];
Window window;

void setup(){
  size(1920,1080,P3D);
  camera(0,0, -(height / 2.0) / tan(PI * 30 / 180), 0, 0, 0, 0 ,-1 ,0);

  spawnBalls();
}

void spawnBalls(){
  
  for(int i = 0; i < numOfBalls; i++){
   
   float colorRed = random(1,255);
   float colorGreen = random(1,255);
   float colorBlue = random(1,255);
   float alpha = random(150,255);
   
   balls[i] = new Mover(Window.left + 50, Window.bottom + 150+(150*i));
   balls[i].setColor(colorRed, colorGreen, colorBlue, alpha);
   balls[i].mass = i +1;
  
  }
}

PVector wind = new PVector(0.1f, 0);
PVector gravity = new PVector(0, -0.5f);


void draw(){
  
 background(255);
 

 for(Mover ball : balls){
   
    ball.applyForce(wind);
   
   if(ball.position.y < window.bottom){
    
     ball.velocity.y *= -1;
     ball.position.y = window.bottom;
     
   }
   
   if(ball.position.x > window.right){
     
      ball.velocity.x *= -1;
      ball.position.x = window.right;
     
   }
   
   if(ball.position.x >= Window.windowWidth/2){
     
    ball.applyFriction();
    
     if(ball.velocity.x <= 0){
        ball.acceleration.x = 0;
        ball.velocity.x = 0; 
        println(ball.velocity.x);
      }
     
   }
  
  
   //ball.applyGravity();
   ball.render();
   ball.update();
   
 }
 
}
