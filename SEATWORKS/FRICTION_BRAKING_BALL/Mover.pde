class Mover{
  
  public PVector position = new PVector();
  public PVector velocity = new PVector();
  public PVector acceleration = new PVector();
  public float scale = 50; 
  public float mass = 1; 
  public float red;
  public float green; 
  public float blue; 
  public float alpha;
  
    Mover()
   {

   }
   
   Mover(float x, float y)
   {
      position = new PVector(x, y);
   }
   
   
   Mover(float x, float y, float scale)
   {
      position = new PVector(x, y);
   }
   
   Mover(PVector position)
   {
      this.position = position;
   }
   
  public void render(){
    
    noStroke();
    fill(red, green, blue, alpha);
    circle(position.x, position.y, this.mass * 10);
    
  }
    public void setColor(float r, float g, float b, float a)
   {
      this.red = r;
      this.green = g;
      this.blue = b;
      this.alpha = a;
   }
  
  public void update(){
    
    this.velocity.add(this.acceleration);
    this.velocity.limit(30);
    this.position.add(this.velocity);
    this.acceleration.mult(0);
  
  }
    
  public void applyForce(PVector force){
   
    //A = F/M
    PVector f = PVector.div(force, this.mass);
    this.acceleration.add(f);
    
  }
  
  public void applyGravity(){
   
    // cancel out the mass
    PVector gravity = new PVector (0,-0.5 * this.mass, 0);
    this.applyForce(gravity);
    
  }
  
  public void applyFriction(float frictionIntensity){
   
    float coefficientFriction = frictionIntensity;
    float normal = 1; 
    float frictionMagnitude = coefficientFriction * normal;
    
    PVector friction = this.velocity.get();
    friction.mult(-1);
    friction.normalize();
    friction.mult(frictionMagnitude);

    this.applyForce(friction);
    
  }
  
  public void applyFriction()
   {
      float c = 0.5; //coefficient of friction
      float normal = 1;
      float frictionMag = c * normal;
      PVector friction = this.velocity.copy(); //gets the copy of the mover's velocity so its value won't be modified
      friction.mult(-1);
      friction.normalize();
      friction.mult(frictionMag);
    
      applyForce(friction);
 }
  
  
  
  
}
