int numOfBalls = 10;
Mover balls[] = new Mover[numOfBalls];

void setup(){
  size(1920,1080,P3D);
  camera(0,0, -(height / 2.0) / tan(PI * 30 / 180), 0, 0, 0, 0 ,-1 ,0);
  createBalls();
}

void createBalls(){
  
   for(int i = 0; i < numOfBalls; i++){
     
   float colorRed = random(1,255);
   float colorGreen = random(1,255);
   float colorBlue = random(1,255);
   float alpha = random(150,255);
   
   balls[i] = new Mover(Window.left + 50, 0);
   balls[i].setColor(colorRed, colorGreen, colorBlue, alpha);
   balls[i].mass = i +1;
  }
  
}


PVector wind = new PVector(0.1, 0);
PVector gravity = new PVector (0,-0.5f);

void draw(){
 
 background(255);
 for(int i=0; i < numOfBalls; i++){
  
   balls[i].update();
   balls[i].render();
   balls[i].applyForce(wind);
   balls[i].applyGravity();
   //gravity.set(0,-0.5 * balls[i].mass);
   //balls[i].applyForce(gravity);
   
   if(balls[i].position.y < Window.bottom){
     
     balls[i].velocity.y *= -1;
     balls[i].position.y = Window.bottom;
     
   }
   
   if(balls[i].position.x > Window.right){

     balls[i].velocity.x *= -1;
     balls[i].position.x = Window.right;
     
   }
   
   if(balls[i].position.x < Window.left){
     
     balls[i].velocity.x *= -1;
     balls[i].position.x = Window.left; 
     
   }
   
 }
  
}
