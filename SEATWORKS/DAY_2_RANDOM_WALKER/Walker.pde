class Walker {

  float xPosition;
  float yPosition;
  
  void render(){
     noStroke();
    circle(xPosition, yPosition, 30);
     
  }
  
  void randomWalk(){
    
    int decision = floor(random(8));
    float red = random(0, 256);
    float green = random(0, 256);
    float blue = random(0, 256);
    float alpha = random(10, 101);
    
   
    
    if (decision == 0){
      
      yPosition+=10;
      fill(red, green, blue, alpha);
     // noStroke();
     
    }
    else if (decision == 1) {
      
       yPosition-=10;
       fill(red, green, blue, alpha);
      // noStroke();
     
    }
    else if (decision == 2) {
      
      xPosition+=10;
      fill(red, green, blue, alpha);
     // noStroke();
      
    } 
    else if (decision == 3){
     
      xPosition-=10;
      fill(red, green, blue, alpha);
     // noStroke();
      
    }
    else if (decision == 4) {
    
      xPosition+=10;
      yPosition+=10;
      fill(red, green, blue, alpha);
     // noStroke();
   
    }
    else if (decision == 5){
     
      xPosition+=10;
      yPosition-=10;
      fill(red, green, blue, alpha);
     // noStroke();
      
    }
    else if (decision == 6){
    
      xPosition-=10;
      yPosition-=10;
      fill(red, green, blue, alpha);
     // noStroke();
      
    }
    else if (decision == 7){
    
      xPosition-=10;
      yPosition+=10;
      fill(red, green, blue, alpha);
     // noStroke();
      
    }
    
    
  }
}
