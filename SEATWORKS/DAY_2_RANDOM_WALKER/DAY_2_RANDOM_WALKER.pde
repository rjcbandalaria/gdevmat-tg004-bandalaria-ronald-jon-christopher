/*

Randomness

lack of pattern 
unpredictability

Goal: 
Create a Mover class 

Creating a Random Number Generator 
Challenge: Random Walker 

*/


void setup(){
  size(1920,1080,P3D);
  camera(0,0, -(height / 2.0) / tan(PI * 30 / 180), 0, 0, 0, 0 ,-1 ,0);
}

Walker walker = new Walker();

void draw(){ 
 
  walker.render();
  walker.randomWalk();
  
}

/*

Uniform distribution  

*/
